! $UWHPSC/codes/fortran/newton/functions.f90

module functions

contains

real(kind=8) function f_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x

    f_sqrt = x**2 - 4.d0

end function f_sqrt


real(kind=8) function fprime_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    fprime_sqrt = 2.d0 * x

end function fprime_sqrt

real(kind=8) function f_out(x)
    implicit none
    real(kind=8), intent(in) :: x
    real, parameter :: pi = 3.141592653589793239

    f_out = x*cos(pi*x) - 1.d0 + 6.d-1 * x**2

end function f_out


real(kind=8) function fprime_out(x)
    implicit none
    real(kind=8), intent(in) :: x
    real, parameter :: pi = 3.141592653589793239
    fprime_out = cos(pi*x) - pi*x*sin(pi*x) + 12.d-1 * x

end function fprime_out

end module functions
