"""
AMATH483
Homework3
Written by Qixiu Hu

This program performs Newton method for square root
It returns a tuple consisting of the final iterate
and the number of iterations
"""

def solve(fvals, x0, debug):
    """
    The main funtion that executes Newton's method
    """

    # intial variables
    x = x0          # pass intial guss
    maxiter = 20    # max interation
    tol = 1.e-14    # tolerance
    
    # print initial guess if debug mode is on
    if debug:
        print "Initial guess: x = %22.15e" % x
    
    # Newton iteration to find a zero of f(x)
    for k in range(1, maxiter + 1): # note k starts from 1 to 20

        # evaluate function and its derivative: 
        fx, fxprime = fvals(x)

        if abs(fx) < tol:
            break

        # Compute Newton increment x:
        deltax = fx/fxprime

        #update x:
        x = x - deltax

        if debug:
            print "After %s iterations, x = %22.15e" % (k,x)

    if k > maxiter:
        # might not have converged

        fx = fvals[0]
        if abs(fx) > tol:
            print "***Warning: has not yet converged"

    # number of iteratoins taken:
    iters = k-1

    return x, iters

def fvals_sqrt(x):
    """
    Return f(x) and f'(x) for applying Newton to find a square root.
    """
    f = x**2 - 4.
    fp = 2.*x
    return f, fp

def test1(debug_solve=False):
    """
    Test Newton iteration for the square root with different initial
    conditions.
    """
    from numpy import sqrt
    for x0 in [1., 2., 100.]:
        print " "  # blank line
        x,iters = solve(fvals_sqrt, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations " % (x,iters)
        fx,fpx = fvals_sqrt(x)
        print "the value of f(x) is %22.15e" % fx
        assert abs(x-2.) < 1e-14, "*** Unexpected result: x = %22.15e"  % x