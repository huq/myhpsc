program intersections

    use newton, only: solve
    use functions, only: f_out, fprime_out

    implicit none
    real(kind=8) :: x, x0, fx
    real(kind=8) :: x0vals(4)
    integer :: iters, itest
	logical :: debug         ! set to .true. or .false.


    ! values to test as x0:
    x0vals = (/-2.d0, -15.d-1, -1.d0, 15.d-1 /)

    do itest=1,4
        x0 = x0vals(itest)
		print *, ' '  ! blank line
        call solve(f_out, fprime_out, x0, x, iters, debug)

        print 11, x0
11      format('With initial guess x0 = ', e22.15)

        print 12, x, iters
12      format('solver returns x = ', e22.15, ' after', i3, ' iterations')

        enddo

end program intersections