from numpy import *
import matplotlib.pyplot as plt
from newton import solve

x = linspace(-5, 5, 1000)
g1 = x * cos(pi * x)
g2 = 1 - 0.6 * x**2

plt.figure(1)       # open plot figure window
plt.clf()           # clear figure
plt.plot(x,g1,'b-')  # connect points with a blue line
plt.plot(x,g2,'r-')  # connect points with a blue line

def fout(x):
    """
    Return f(x) and f'(x) for applying Newton to find a square root.
    """
    g1 = x * cos(pi * x)
    g2 = 1 - 0.6 * x**2
    f = g1 - g2
    fp = cos(pi*x) - pi*x*sin(pi*x) + 1.2*x
    return f, fp

x1, iters1 = solve(fout, -2, debug=False)
print "With initial guess x0 = %22.15e ," % -2
print "solve returns x = %22.15e after %i iterations " % (x1,iters1)
y1 = 1 - 0.6 * x1**2
plt.plot(x1,y1,'ko')

print ""
x2, iters2 = solve(fout, -1.5, debug=False)
print "With initial guess x0 = %22.15e ," % -1.5
print "solve returns x = %22.15e after %i iterations " % (x2,iters2)
y2 = 1 - 0.6 * x2**2
plt.plot(x2,y2,'ko')

print ""
x3, iters3 = solve(fout, -1, debug=False)
print "With initial guess x0 = %22.15e ," % -1
print "solve returns x = %22.15e after %i iterations " % (x3,iters3)
y3 = 1 - 0.6 * x3**2
plt.plot(x3,y3,'ko')

print ""
x4, iters4 = solve(fout, 1.5, debug=False)
print "With initial guess x0 = %22.15e ," % 1.5
print "solve returns x = %22.15e after %i iterations " % (x4,iters4)
y4 = 1 - 0.6 * x4**2
plt.plot(x4,y4,'ko')

plt.title("Data points and g1, g2")
plt.savefig('intersections.png')   # save figure as .png file
