module quadrature

contains

real(kind=8) function trapezoid(f, a, b, n)
    implicit none

	! input varialbles:
    real(kind=8), external :: f
	real(kind=8), intent(in) :: a, b
	integer, intent(in) :: n

	! local variables:
	real(kind=8), dimension(1:n) :: xj, fj
	real(kind=8) :: h, trap_sum
	integer :: i,j

    h = (b-a)/(n-1)

	do i=1,n
		xj(i) = a + (i-1)*h
		fj(i) = f(xj(i))
	enddo

	trap_sum = 0.d0

	do j=1,n
		trap_sum = fj(j) + trap_sum
	enddo

	trapezoid = h*trap_sum - 0.5*h*(fj(1) + fj(n))

end function trapezoid

subroutine error_table(f, a, b, nvals, int_true)
	implicit none

	! input varialbles:
    real(kind=8), external :: f
	real(kind=8), intent(in) :: a, b, int_true
	integer, dimension(:), intent(in) :: nvals

	! local variables:
	real(kind=8) :: int_trap, error, ratio, last_error
	integer :: i, n

	print *, "    n         trapezoid            error       ratio"

	n = size(nvals)
	last_error = 0

	do i=1,n
		int_trap = trapezoid(f,a,b,nvals(i))
		error = abs(int_trap - int_true)
        ratio = last_error / error
        last_error = error

		print 11, nvals(i), int_trap, error, ratio
		11     format(i8, es22.14, es13.3, es13.3)
	enddo

end subroutine error_table

end module quadrature