module quadrature_mc

contains

real(kind=8) function quad_mc(g, a, b, ndim, npoints)
    implicit none

    ! input varialbles:
    real(kind=8), external :: g
    integer, intent(in) :: ndim, npoints
    real(kind=8), dimension(ndim), intent(in) :: a, b
    
    ! local variables:
    real(kind=8) :: volume, gtotal
    real(kind=8), allocatable:: rand(:)
    real(kind=8), dimension(npoints) :: gx
    integer :: i, j, m
    
    volume = product(b-a)
    
    allocate(rand(npoints*ndim))
    call random_number(rand)
    rand = 2*(rand+1)

    gtotal = 0
    do i=1, npoints
        gtotal = g(rand((i-1)*ndim+1:ndim*i),ndim) + gtotal
    enddo

    quad_mc = (volume/npoints)*gtotal
    !quad_mc = 195734186
    
end function quad_mc

end module quadrature_mc