! AMATH583 Final Project
! Written by Qixiu Hu

program laplace_mc

    use problem_description
    use mc_walk, only: many_walks, nwalks
    use random_util

    ! declare variables
    implicit none
    integer :: i0, j0, max_steps, seed1, n_mc, n_success, i, n_total
    real(kind=8) :: x0, y0, u_mc, error, u_mc_total, u_sum_new, u_sum_old, u_true

	open(unit=25, file='mc_laplace_error.txt', status='unknown')

    seed1 = 12345   ! or set to 0 for random seed
    call init_random_seed(seed1)

    ! Try it out from a specific (x0,y0):
    x0 = 0.9
    y0 = 0.6

    i0 = nint((x0-ax)/dx)
    j0 = nint((y0-ay)/dy)

    ! shift (x0,y0) to a grid point if it wasn't already:
    x0 = ax + i0*dx
    y0 = ay + j0*dy

    u_true = utrue(x0,y0)

    max_steps = 100*max(nx,ny)
	!max_steps = 10

    n_mc = 10
    call many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)

	u_mc_total = u_mc
    n_total = n_success

    error = abs((u_mc - u_true) / u_true)
	write(25,'(i10,e23.15,e15.6)') n_total, u_mc_total, error

    print 11, n_success, u_mc, error
 11 format(i10, '  ', e22.15, '  ', e22.6)

    do i=1, 12
		!n_mc = 2*n_mc
        u_sum_old = u_mc_total * n_total
        call many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)
        u_sum_new = u_mc * n_success
        n_total = n_total + n_success
        u_mc_total = (u_sum_old + u_sum_new) / n_total
        error = abs((u_mc_total - u_true) / u_true)

		write(25,'(i10,e23.15,e15.6)') n_total, u_mc_total, error

        print 12, n_total, u_mc_total, error
 12 format(i10, '  ', e22.15, '  ', e22.6)

        n_mc = 2*n_mc
    enddo

    print 13, u_mc_total
 13 format('Final approximation to u(x0,y0):   ', es22.14)

    print 14, nwalks
 14 format('Total number of random walks:      ', i10)

end program laplace_mc