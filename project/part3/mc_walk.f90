! AMATH583 Final Project
! Written by Qixiu Hu

module mc_walk

use problem_description

implicit none

integer :: nwalks

contains

subroutine random_walk(i0, j0, max_steps, ub, iabort)
	implicit none

	! input varialbles:
	integer, intent(in) :: i0, j0, max_steps

	! output variables:
	integer, intent(out) :: iabort
	real(kind=8), intent(out) :: ub

	! local variables:
	real(kind=8), allocatable:: rand(:)
	real(kind=8) :: xb, yb
	integer :: i, j, istep

	! starting point
	i = i0
	j = j0

	allocate(rand(max_steps))
	call random_number(rand)

	do istep=1, max_steps

		! Take the next random step with equal probability in each
        ! direction:
		if (rand(istep) < 0.25) then
			i = i-1		!step left
		else if (rand(istep) < 0.5) then
			i = i+1		!step right
		else if (rand(istep) <0.75) then
			j = j-1		!step down
		else 
			j = j+1 	!step up
		endif

		if (i*j*(nx+1-i)*(ny+1-j) == 0) then
			xb = ax + i*dx
            yb = ay + j*dy
            ub = uboundary(xb, yb)

			iabort = 0
			go to 99
		endif

		if (istep == max_steps-1) then		
			iabort = 1
		endif
	enddo

99 continue

end subroutine random_walk

subroutine many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)
	implicit none

	! input varialbles:
	integer, intent(in) :: i0, j0, max_steps, n_mc

	! output variables:
	integer, intent(out) :: n_success
	real(kind=8), intent(out) :: u_mc

	! local variables:
	real(kind=8), allocatable:: rand(:)
	real(kind=8) :: ub_sum, ub
	integer :: i, j, k, iabort

	ub_sum = 0
	n_success = 0

	do k=1,n_mc
		i = i0
		j = j0
		call random_walk(i0, j0, max_steps, ub, iabort)
		nwalks = nwalks + 1

		if (iabort == 0) then
			ub_sum = ub_sum + ub
			n_success = n_success + 1
		endif
	enddo

	u_mc = ub_sum / n_success
	
end subroutine many_walks

end module mc_walk